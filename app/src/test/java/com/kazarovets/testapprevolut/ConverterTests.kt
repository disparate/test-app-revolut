package com.kazarovets.testapprevolut

import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.logic.usecase.CurrencyConverter
import org.junit.Test
import java.lang.IllegalArgumentException
import java.math.BigDecimal

class ConverterTests {

    private val converter = CurrencyConverter()

    @Test(expected = IllegalArgumentException::class)
    fun checkDifferentBaseThrowError() {
        val one = BigDecimal.ONE
        val selected = CurrencyRate(AppCurrency.EURO, AppCurrency.AUSTRALIAN_DOLLAR, one)
        val currency = CurrencyRate(AppCurrency.CANADIAN_DOLLAR, AppCurrency.BOLGARIAN_LEV, one)

        converter.convertCurrency(one, selected, currency)
    }

    @Test
    fun checkConvert() {
        checkResult(1.0, 1.0, 2.0, 2.0)
        checkResult(1.0, 2.0, 1.0, 0.5)
        checkResult(0.5, 0.5, 1.3, 1.3)
        checkResult(1.5, 2.0, 2.0, 1.5)
        checkResult(1.0, 1.0, 0.0, 0.0)
        checkResult(0.3, 0.5, 0.25, 0.15)
    }

    @Test(expected = IllegalArgumentException::class)
    fun checkSelectedRateZero() {
        checkResult(1.0, 0.0, 1.0, 1.0)
    }


    private fun checkResult(
        selectedValue: BigDecimal,
        selectedRateValue: BigDecimal,
        currencyRateValue: BigDecimal,
        result: BigDecimal
    ) {
        val selectedRate =
            CurrencyRate(AppCurrency.CANADIAN_DOLLAR, AppCurrency.EURO, selectedRateValue)
        val currencyRate =
            CurrencyRate(AppCurrency.BOLGARIAN_LEV, AppCurrency.EURO, currencyRateValue)

        val realRes = converter.convertCurrency(selectedValue, selectedRate, currencyRate).stripTrailingZeros()

        assert(realRes.checkValueEqual(result)) { "selected val $selectedValue, rate $selectedRateValue gave $realRes for $currencyRateValue, expected $result" }
    }

    private fun checkResult(
        selectedValue: Double,
        selectedRateValue: Double,
        currencyRateValue: Double,
        result: Double
    ) {
        fun Double.toBigDecimal() = BigDecimal.valueOf(this)
        checkResult(
            selectedValue.toBigDecimal(),
            selectedRateValue.toBigDecimal(),
            currencyRateValue.toBigDecimal(),
            result.toBigDecimal()
        )
    }

    private fun BigDecimal.checkValueEqual(decimal: BigDecimal): Boolean {
        return this.compareTo(decimal) == 0
    }
}