package com.kazarovets.testapprevolut

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.logic.model.CurrencyRatesResponse
import com.kazarovets.testapprevolut.logic.usecase.GetCurrencyRatesInteractor
import com.kazarovets.testapprevolut.logic.usecase.SelectCurrencyInteractor
import com.kazarovets.testapprevolut.logic.usecase.UpdateCurrenciesInteractor
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import com.kazarovets.testapprevolut.ui.recycler.CurrenciesUpdate
import com.kazarovets.testapprevolut.ui.vm.RatesListViewModel
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*
import java.math.BigDecimal
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class RatesViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val testScheduler = TestScheduler()

    private val testSchedulers = object : AppSchedulers {
        override fun ioScheduler() = testScheduler
        override fun computationScheduler() = testScheduler
        override fun uiScheduler() = testScheduler
    }

    private val observerUpdate: Observer<CurrenciesUpdate> = mock()
    private val observerError: Observer<Boolean> = mock()

    private val getInteractor: GetCurrencyRatesInteractor = mock()
    private val updateInteractor: UpdateCurrenciesInteractor = mock()
    private val selectInteractor: SelectCurrencyInteractor = mock()

    private val base = AppCurrency.EURO
    private val initInfos = listOf(CurrencyViewInfo(base, 0, 0, BigDecimal.ONE, false))
    private val rates = listOf(CurrencyRate(base, base, BigDecimal.ONE))

    private lateinit var viewModel: RatesListViewModel

    private fun createVM() {
        viewModel =
            RatesListViewModel(getInteractor, updateInteractor, selectInteractor, testSchedulers)

        viewModel.currenciesInfos.observeForever(observerUpdate)
        viewModel.errorDownloadingRates.observeForever(observerError)
    }

    @Test
    fun checkInitValues() {
        mockInitValues()

        doReturn(Observable.never<CurrencyRatesResponse>()).`when`(getInteractor).getRatesStream()

        createVM()
        assert(viewModel.currenciesInfos.value == CurrenciesUpdate(initInfos))
        assert(viewModel.errorDownloadingRates.value == false)

        viewModel.onCleared()
    }

    @Test
    fun checkErrorStateUpdate() {
        mockInitValues()

        val updatedInfos = initInfos + initInfos
        doReturn(updatedInfos).`when`(updateInteractor)
            .updateCurrenciesWithNewRates(base, initInfos, rates)

        val error = SocketTimeoutException()
        doReturn(Observable.interval(0, 1, TimeUnit.SECONDS, testScheduler)
            .map {
                if (it == 0L) CurrencyRatesResponse.ErrorLoading(error)
                else CurrencyRatesResponse.Success(rates)
            }
        ).`when`(getInteractor).getRatesStream()

        createVM()

        testScheduler.advanceTimeTo(100, TimeUnit.MILLISECONDS)
        assert(viewModel.currenciesInfos.value == CurrenciesUpdate(initInfos))
        assert(viewModel.errorDownloadingRates.value == true)


        testScheduler.advanceTimeTo(1100, TimeUnit.MILLISECONDS)
        assert(viewModel.currenciesInfos.value == CurrenciesUpdate(updatedInfos))
        assert(viewModel.errorDownloadingRates.value == false)

        viewModel.onCleared()
    }

    @Test
    fun testOnItemClick() {
        mockInitValues()
        doReturn(Observable.never<CurrencyRatesResponse>()).`when`(getInteractor)
            .getRatesStream()

        val new = AppCurrency.NEW_ZELLAND_DOLLAR
        val update = CurrenciesUpdate(initInfos + initInfos)
        doReturn(update).`when`(selectInteractor).selectCurrency(new, base, initInfos)

        createVM()

        viewModel.onItemClick(new)

        verify(selectInteractor, times(1)).selectCurrency(new, base, initInfos)

        assert(viewModel.currenciesInfos.value == update)

        viewModel.onCleared()
    }

    @Test
    fun checkOnSelectedCurrencyValueChanged() {
        assertOnSelectedValueChanged("10", BigDecimal("10"))
    }

    @Test
    fun checkOnSelectedCurrencyValueNull() {
        assertOnSelectedValueChanged(null, BigDecimal.ZERO)
    }

    @Test
    fun checkOnSelectedCurrencyIncorrect() {
        assertOnSelectedValueChanged("incorrect", BigDecimal.ZERO)
    }

    private fun assertOnSelectedValueChanged(text: String?, decimal: BigDecimal) {
        mockInitValues()
        doReturn(Observable.never<CurrencyRatesResponse>()).`when`(getInteractor)
            .getRatesStream()

        createVM()

        val updatedInfos = initInfos + initInfos
        doReturn(updatedInfos).`when`(updateInteractor).updateCurrenciesWithNewSelectedValue(
            base, decimal, initInfos, emptyList()
        )

        viewModel.onSelectedCurrencyValueChanged(text)

        verify(updateInteractor, times(1)).updateCurrenciesWithNewSelectedValue(
            base, decimal, initInfos, emptyList()
        )
        assert(viewModel.currenciesInfos.value == CurrenciesUpdate(updatedInfos))
    }


    private fun mockInitValues() {
        doReturn(base).`when`(selectInteractor).getStartSelectedCurrency()
        doReturn(initInfos).`when`(getInteractor).initCurrencyRatesValues(base)
    }
}