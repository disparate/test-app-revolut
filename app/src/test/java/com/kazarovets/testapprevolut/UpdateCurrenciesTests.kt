package com.kazarovets.testapprevolut

import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.logic.usecase.CurrencyConverter
import com.kazarovets.testapprevolut.logic.usecase.UpdateCurrenciesInteractorImpl
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import org.junit.Test
import java.math.BigDecimal

class UpdateCurrenciesTests {

    val interactor = UpdateCurrenciesInteractorImpl(
        CurrencyConverter()
    )

    val rep = AppCurrency.INDEAN_RUPEE
    val aud = AppCurrency.AUSTRALIAN_DOLLAR
    val lev = AppCurrency.BOLGARIAN_LEV

    @Test
    fun checkUpdateWithNewRates() {
        val newRates = listOf(
            currencyRate(rep, 1.0),
            currencyRate(aud, 2.0),
            currencyRate(lev, 3.0)
        )
        val infos = listOf(
            currencyInfo(rep, 2.0, false),
            currencyInfo(aud, 10.0, true),
            currencyInfo(lev, 0.1, false)
        )
        val new = interactor.updateCurrenciesWithNewRates(
            rep, infos, newRates
        )
        val expected = infos.map {
            val newVal = when (it.currency) {
                rep -> 2.0
                aud -> 4.0
                lev -> 6.0
                else -> 0.0
            }
            currencyInfo(it.currency, newVal, it.isEditable)
        }
        assertInfosListsAreSame(new, expected)
    }

    @Test
    fun checkUpdateWithoutSomeRates() {
        val newRates = listOf(
            currencyRate(rep, 1.0),
            currencyRate(aud, 2.0)
        )
        val infos = listOf(
            currencyInfo(rep, 2.0, false),
            currencyInfo(aud, 10.0, true),
            currencyInfo(lev, 0.1, false)
        )
        val new = interactor.updateCurrenciesWithNewRates(
            rep, infos, newRates
        )
        assert(new.size == newRates.size)
        assert(new.any { it.currency == rep } && new.any { it.currency == aud })
    }

    @Test
    fun checkUpdateWithoutSomeInfos() {
        val newRates = listOf(
            currencyRate(rep, 1.0),
            currencyRate(aud, 2.0),
            currencyRate(lev, 3.0)
        )
        val infos = listOf(
            currencyInfo(rep, 2.0, false),
            currencyInfo(lev, 0.1, false)
        )
        val new = interactor.updateCurrenciesWithNewRates(
            rep, infos, newRates
        )
        assert(new.size == infos.size)
        assert(new.any { it.currency == rep } && new.any { it.currency == lev })
    }

    @Test
    fun checkUpdateWithNewValue() {
        val rates = listOf(
            currencyRate(rep, 1.0),
            currencyRate(aud, 2.0),
            currencyRate(lev, 3.0)
        )
        val infos = listOf(
            currencyInfo(rep, 2.0, false),
            currencyInfo(aud, 10.0, true),
            currencyInfo(lev, 0.1, false)
        )

        val newValue = BigDecimal(3.0)

        val new = interactor.updateCurrenciesWithNewSelectedValue(
            lev, newValue, infos, rates
        )

        val expected = infos.map {
            val newVal = when (it.currency) {
                rep -> 1.0
                aud -> 2.0
                lev -> 3.0
                else -> 0.0
            }
            currencyInfo(it.currency, newVal, it.isEditable)
        }

        assertInfosListsAreSame(new, expected)
    }

    private fun assertInfosListsAreSame(
        expected: List<CurrencyViewInfo>,
        real: List<CurrencyViewInfo>
    ) {
        assert(expected.size == real.size) { "different number of elements" }

        var isSame = true
        expected.forEachIndexed { i, value ->
            val fromReal = real[i]
            if (value.isEditable != fromReal.isEditable
                || value.currency != fromReal.currency
                || value.value.compareTo(fromReal.value) != 0
            ) {
                isSame = false
            }
        }
        assert(isSame) { "lists are not same" }
    }


    private fun currencyInfo(
        currency: AppCurrency,
        value: Double,
        isEditable: Boolean = false
    ): CurrencyViewInfo {
        return CurrencyViewInfo(currency, 0, 0, BigDecimal.valueOf(value), isEditable)
    }

    private fun currencyRate(currency: AppCurrency, value: Double): CurrencyRate {
        return CurrencyRate(currency, rep, BigDecimal.valueOf(value))
    }


}