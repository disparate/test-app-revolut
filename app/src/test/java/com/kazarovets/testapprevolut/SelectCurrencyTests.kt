package com.kazarovets.testapprevolut

import com.kazarovets.testapprevolut.data.RatesRepository
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.usecase.SelectCurrencyInteractor
import com.kazarovets.testapprevolut.logic.usecase.SelectCurrencyInteractorImpl
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import org.junit.Test
import org.mockito.Mockito.*
import java.math.BigDecimal

class SelectCurrencyTests {

    val repo: RatesRepository = mock()
    val interactor = SelectCurrencyInteractorImpl(repo)

    val rep = AppCurrency.INDEAN_RUPEE
    val aud = AppCurrency.AUSTRALIAN_DOLLAR
    val lev = AppCurrency.BOLGARIAN_LEV


    val repInfo = currencyInfo(rep, 2.0, true)
    val audInfo = currencyInfo(aud, 1.0, false)
    val levInfo = currencyInfo(lev, 0.1, true)

    val infos = listOf(repInfo, audInfo, levInfo)

    @Test
    fun checkSameCurrencyReturns() {
        val update = interactor.selectCurrency(rep, rep, infos)

        assert(update == null)
        verifyZeroInteractions(repo)
    }

    @Test
    fun checkConversion() {
        checkConversion(rep, aud, infos.makeOnlyFirstEditable())
        checkConversion(aud, rep, listOf(audInfo, repInfo, levInfo).makeOnlyFirstEditable())
        checkConversion(lev, rep, listOf(levInfo, repInfo, audInfo).makeOnlyFirstEditable())
    }

    @Test
    fun checkNoSelectedReturns() {
        val update = interactor.selectCurrency(AppCurrency.CANADIAN_DOLLAR, rep, infos)

        assert(update == null)
        verifyZeroInteractions(repo)
    }

    private fun checkConversion(
        selected: AppCurrency,
        prev: AppCurrency,
        expected: List<CurrencyViewInfo>
    ) {

        val update = interactor.selectCurrency(selected, prev, infos)

        assert(update != null)
        assert(update!!.selectedRateChanged)
        assert(update!!.infos == expected) { " should be $expected, was ${update!!.infos}" }

        verify(repo, times(1)).saveSelectedCurrency(rep)
    }


    private fun currencyInfo(
        currency: AppCurrency,
        value: Double,
        isEditable: Boolean = false
    ): CurrencyViewInfo {
        return CurrencyViewInfo(currency, 0, 0, BigDecimal.valueOf(value), isEditable)
    }

    private fun List<CurrencyViewInfo>.makeOnlyFirstEditable(): List<CurrencyViewInfo> {
        return this.mapIndexed { index, currencyViewInfo ->
            if (index == 0) currencyViewInfo.editable()
            else currencyViewInfo.notEditable()
        }
    }

    private fun CurrencyViewInfo.editable(): CurrencyViewInfo {
        return this.copy(isEditable = true)
    }

    private fun CurrencyViewInfo.notEditable(): CurrencyViewInfo {
        return this.copy(isEditable = false)
    }
}