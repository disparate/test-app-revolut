package com.kazarovets.testapprevolut

import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.data.RatesRepository
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.logic.model.CurrencyRatesResponse
import com.kazarovets.testapprevolut.logic.usecase.AppLogicConstants
import com.kazarovets.testapprevolut.logic.usecase.GetCurrencyRatesInteractorImpl
import com.kazarovets.testapprevolut.ui.vm.CurrencyViewInfoMapper
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doReturn
import java.math.BigDecimal
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit


class GetCurrencyRatesTests {

    val repo: RatesRepository = mock()

    val testScheduler = TestScheduler()

    val testSchedulers = object : AppSchedulers {
        override fun ioScheduler() = testScheduler
        override fun computationScheduler() = testScheduler
        override fun uiScheduler() = testScheduler
    }

    val interactor = GetCurrencyRatesInteractorImpl(repo, CurrencyViewInfoMapper(), testSchedulers)

    val base = AppLogicConstants.baseCurrency

    val cr1 = CurrencyRate(base, base, BigDecimal.ONE)
    val cr2 = CurrencyRate(AppCurrency.BOLGARIAN_LEV, base, BigDecimal.ONE)

    private val singleCr1 = Single.just(listOf(cr1))
    private val singleCr2 = Single.just(listOf(cr2))

    @Test
    fun checkLocalAndThenRemote() {

        mockLocal(singleCr1)
        mockRemote(singleCr2.delay(1300, TimeUnit.MILLISECONDS, testScheduler))

        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(1500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(2)

        testObserver.assertStillRunningAndDispose()
    }


    @Test
    fun checkLocalAndThenRemoteError() {
        mockLocal(singleCr1)

        val errorLoading = SocketTimeoutException()
        mockRemote(singleCr2
            .delay(1300, TimeUnit.MILLISECONDS, testScheduler)
            .map<List<CurrencyRate>> { throw errorLoading }
        )

        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(1500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(2)
        testObserver.assertValueAt(1, CurrencyRatesResponse.ErrorLoading(errorLoading))

        testObserver.assertStillRunningAndDispose()
    }

    @Test
    fun checkThrottle() {

        mockLocal(singleCr1)
        mockRemote(singleCr2.delay(300, TimeUnit.MILLISECONDS, testScheduler))

        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testObserver.assertStillRunningAndDispose()
    }

    @Test
    fun checkDistinct() {

        mockLocal(singleCr1)
        mockRemote(singleCr1.delay(1300, TimeUnit.MILLISECONDS, testScheduler))


        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(1500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testObserver.assertStillRunningAndDispose()
    }


    @Test
    fun checkIgnoreLocalEmpty() {
        mockLocal(Single.just(emptyList()))
        mockRemote(singleCr1.delay(300, TimeUnit.MILLISECONDS, testScheduler))


        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(0)

        testScheduler.advanceTimeTo(500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testObserver.assertStillRunningAndDispose()
    }


    @Test
    fun checkIgnoreLocalError() {
        mockLocal(Single.just(emptyList()))
        mockRemote(singleCr1.delay(300, TimeUnit.MILLISECONDS, testScheduler))

        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(200, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(0)

        testScheduler.advanceTimeTo(500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testObserver.assertStillRunningAndDispose()
    }

    @Test
    fun checkReturnAfterError() {

        mockLocal(Single.just(emptyList()))
        val error = SocketTimeoutException()

        mockRemote(
            listOf(
                singleCr2,
                singleCr2.map<List<CurrencyRate>> { throw error },
                singleCr2.map<List<CurrencyRate>> { throw error },
                singleCr2
            )
        )


        val testObserver = createTestObserver()

        testScheduler.advanceTimeTo(100, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testScheduler.advanceTimeTo(1500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(2)

        testScheduler.advanceTimeTo(4000, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(3)

        testObserver.assertStillRunningAndDispose()
    }

    private fun mockLocal(single: Single<List<CurrencyRate>>) {
        doReturn(single).`when`(repo).getSavedCurrencyRates()
    }

    private fun mockRemote(single: Single<List<CurrencyRate>>) {
        `when`(repo.downloadAndSaveCurrencyRates(base)).thenReturn(single)
    }

    private fun mockRemote(singles: List<Single<List<CurrencyRate>>>) {
        var stub = `when`(repo.downloadAndSaveCurrencyRates(base))

        singles.forEach {
            stub = stub.thenReturn(it)
        }
    }


    private fun createTestObserver(): TestObserver<CurrencyRatesResponse> {
        val observer = interactor.getRatesStream()
            .subscribeOn(testScheduler)
            .observeOn(testScheduler)
            .test()


        observer.assertNotTerminated()
            .assertNoErrors()
            .assertValueCount(0)

        return observer
    }

    private fun TestObserver<CurrencyRatesResponse>.assertStillRunningAndDispose() {
        this.assertNoErrors()
            .assertNotTerminated()

        this.dispose()
    }
}