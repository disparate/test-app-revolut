package com.kazarovets.testapprevolut.ui.recycler

import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo

data class CurrenciesUpdate(
    val infos: List<CurrencyViewInfo>,
    val selectedRateChanged: Boolean = false
)