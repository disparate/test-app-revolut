package com.kazarovets.testapprevolut.ui.recycler

import androidx.recyclerview.widget.DiffUtil
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo

class RatesListDiffCallback : DiffUtil.ItemCallback<CurrencyViewInfo>() {
    override fun areItemsTheSame(oldItem: CurrencyViewInfo, newItem: CurrencyViewInfo): Boolean {
        return oldItem.currency == newItem.currency
    }

    override fun areContentsTheSame(oldItem: CurrencyViewInfo, newItem: CurrencyViewInfo): Boolean {
        return oldItem == newItem
    }

    override fun getChangePayload(oldItem: CurrencyViewInfo, newItem: CurrencyViewInfo): Any? {
        return PAYLOAD_CHANGE_VALUE_AND_EDITABLE
    }

    companion object {
        const val PAYLOAD_CHANGE_VALUE_AND_EDITABLE = 1
    }
}