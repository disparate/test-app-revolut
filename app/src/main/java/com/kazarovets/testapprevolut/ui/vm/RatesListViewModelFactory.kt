package com.kazarovets.testapprevolut.ui.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.usecase.UpdateCurrenciesInteractor
import com.kazarovets.testapprevolut.logic.usecase.GetCurrencyRatesInteractor
import com.kazarovets.testapprevolut.logic.usecase.SelectCurrencyInteractor
import javax.inject.Inject

@RatesScope
class RatesListViewModelFactory @Inject constructor(
    private val getCurrencyRatesInteractor: GetCurrencyRatesInteractor,
    private val convertCurrencyInteractor: UpdateCurrenciesInteractor,
    private val selectCurrencyInteractor: SelectCurrencyInteractor,
    private val appSchedulers: AppSchedulers
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RatesListViewModel(
            getCurrencyRatesInteractor,
            convertCurrencyInteractor,
            selectCurrencyInteractor,
            appSchedulers
        ) as T
    }
}