package com.kazarovets.testapprevolut.ui.vm

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.kazarovets.testapprevolut.R
import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import java.math.BigDecimal
import javax.inject.Inject

@RatesScope
class CurrencyViewInfoMapper @Inject constructor() {

    fun mapToViewInfo(
        currency: AppCurrency,
        value: BigDecimal,
        isEditable: Boolean
    ): CurrencyViewInfo {
        return CurrencyViewInfo(
            currency,
            getCurrencyNameRes(currency),
            getImageRes(currency),
            value, isEditable
        )
    }

    @StringRes
    private fun getCurrencyNameRes(currency: AppCurrency): Int {
        return when (currency) {
            AppCurrency.EURO -> R.string.currency_name_euro
            AppCurrency.AUSTRALIAN_DOLLAR -> R.string.currency_name_australian_dollar
            AppCurrency.BOLGARIAN_LEV -> R.string.currency_name_bolgarian_lev
            AppCurrency.BRAZILIAN_REAL -> R.string.currency_name_brazilian_real
            AppCurrency.CANADIAN_DOLLAR -> R.string.currency_name_canadian_dollar
            AppCurrency.SUISS_FRANK -> R.string.currency_name_suiss_frank
            AppCurrency.CHINESS_RENMINBI -> R.string.currency_name_chiness_renminbi
            AppCurrency.CZECH_CROWN -> R.string.currency_name_czech_crown
            AppCurrency.DEUTCH_CROWN -> R.string.currency_name_deutch_crown
            AppCurrency.GREAT_BRITAIN_POUND -> R.string.currency_name_great_britain_pound
            AppCurrency.HONGKONG_DOLLAR -> R.string.currency_name_hongkong_dollar
            AppCurrency.CROATIAN_KUNA -> R.string.currency_name_croatian_kuna
            AppCurrency.HUNGARIAN_FORINT -> R.string.currency_name_hungarian_forint
            AppCurrency.INDONESIAN_RUPIAH -> R.string.currency_name_indonesian_rupiah
            AppCurrency.ISRAEL_SHEKEL -> R.string.currency_name_israel_shekel
            AppCurrency.INDEAN_RUPEE -> R.string.currency_name_indean_rupee
            AppCurrency.ICELANDIC_KRONA -> R.string.currency_name_icelandic_krona
            AppCurrency.JAPANESE_YENA -> R.string.currency_name_japanese_yena
            AppCurrency.SOUTH_KOREAN_WON -> R.string.currency_name_south_korean_won
            AppCurrency.MEXICAN_PESO -> R.string.currency_name_mexican_peso
            AppCurrency.MALAYSIAN_RINGIT -> R.string.currency_name_malaysian_ringit
            AppCurrency.NORWEGIAN_KRONE -> R.string.currency_name_norwegian_krone
            AppCurrency.NEW_ZELLAND_DOLLAR -> R.string.currency_name_new_zelland_dollar
            AppCurrency.PHILIPPINE_PESO -> R.string.currency_name_philippine_peso
            AppCurrency.POLISH_ZLOTYI -> R.string.currency_name_polish_zlotyi
            AppCurrency.ROMANIAL_LEU -> R.string.currency_name_romanial_leu
            AppCurrency.RUSSIAN_ROUBLE -> R.string.currency_name_russian_rouble
            AppCurrency.SWEDISH_KRONA -> R.string.currency_name_swedish_krona
            AppCurrency.SINGAPORE_DOLLAR -> R.string.currency_name_singapore_dollar
            AppCurrency.THAI_BAHT -> R.string.currency_name_thai_baht
            AppCurrency.TURKISH_LIRA -> R.string.currency_name_turkish_lira
            AppCurrency.SOUTH_AFRICAN_RAND -> R.string.currency_name_south_africa_rand
        }
    }

    @DrawableRes
    private fun getImageRes(currency: AppCurrency): Int {
        return when (currency) {
            AppCurrency.EURO -> R.drawable.european_union
            AppCurrency.AUSTRALIAN_DOLLAR -> R.drawable.australia
            AppCurrency.BOLGARIAN_LEV -> R.drawable.bulgaria
            AppCurrency.BRAZILIAN_REAL -> R.drawable.brazil
            AppCurrency.CANADIAN_DOLLAR -> R.drawable.canada
            AppCurrency.SUISS_FRANK -> R.drawable.switzerland
            AppCurrency.CHINESS_RENMINBI -> R.drawable.china
            AppCurrency.CZECH_CROWN -> R.drawable.czech_republic
            AppCurrency.DEUTCH_CROWN -> R.drawable.denmark
            AppCurrency.GREAT_BRITAIN_POUND -> R.drawable.united_kingdom
            AppCurrency.HONGKONG_DOLLAR -> R.drawable.hong_kong
            AppCurrency.CROATIAN_KUNA -> R.drawable.croatia
            AppCurrency.HUNGARIAN_FORINT -> R.drawable.hungary
            AppCurrency.INDONESIAN_RUPIAH -> R.drawable.indonesia
            AppCurrency.ISRAEL_SHEKEL -> R.drawable.israel
            AppCurrency.INDEAN_RUPEE -> R.drawable.india
            AppCurrency.ICELANDIC_KRONA -> R.drawable.iceland
            AppCurrency.JAPANESE_YENA -> R.drawable.japan
            AppCurrency.SOUTH_KOREAN_WON -> R.drawable.south_korea
            AppCurrency.MEXICAN_PESO -> R.drawable.mexico
            AppCurrency.MALAYSIAN_RINGIT -> R.drawable.malaysia
            AppCurrency.NORWEGIAN_KRONE -> R.drawable.norway
            AppCurrency.NEW_ZELLAND_DOLLAR -> R.drawable.new_zealand
            AppCurrency.PHILIPPINE_PESO -> R.drawable.philippines
            AppCurrency.POLISH_ZLOTYI -> R.drawable.republic_of_poland
            AppCurrency.ROMANIAL_LEU -> R.drawable.romania
            AppCurrency.RUSSIAN_ROUBLE -> R.drawable.russia
            AppCurrency.SWEDISH_KRONA -> R.drawable.sweden
            AppCurrency.SINGAPORE_DOLLAR -> R.drawable.singapore
            AppCurrency.THAI_BAHT -> R.drawable.thailand
            AppCurrency.TURKISH_LIRA -> R.drawable.turkey
            AppCurrency.SOUTH_AFRICAN_RAND -> R.drawable.south_africa
        }
    }

}