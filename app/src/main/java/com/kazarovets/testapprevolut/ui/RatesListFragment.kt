package com.kazarovets.testapprevolut.ui

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kazarovets.testapprevolut.R
import com.kazarovets.testapprevolut.di.rates.DaggerRatesComponent
import com.kazarovets.testapprevolut.di.rates.RatesModule
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.ui.recycler.RatesListRecyclerAdapter
import com.kazarovets.testapprevolut.ui.vm.RatesListViewModel
import com.kazarovets.testapprevolut.ui.vm.RatesListViewModelFactory
import kotlinx.android.synthetic.main.rates_fragment.*
import javax.inject.Inject

class RatesListFragment : Fragment() {

    @Inject
    protected lateinit var viewModelProvider: RatesListViewModelFactory

    protected lateinit var viewModel: RatesListViewModel

    private val adapter = RatesListRecyclerAdapter(
        onClick = ::onItemClick,
        onSelectedItemValueChange = ::onSelectedItemValueChanged
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.rates_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        injectDependencies(view.context)

        setupRecycler()

        setupDownloadingError()

    }

    private fun setupRecycler() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ratesListRecycler.layoutManager = layoutManager
        ratesListRecycler.adapter = adapter

        viewModel.currenciesInfos.observe(this, Observer {
            adapter.submitList(it.infos) {
                if (it.selectedRateChanged) {
                    ratesListRecycler.scrollToPosition(0)
                }
            }
        })
    }

    private fun setupDownloadingError() {
        viewModel.errorDownloadingRates.observe(this, Observer {
            cantUpdateText.isVisible = it
        })
    }

    private fun onItemClick(item: AppCurrency) {
        viewModel.onItemClick(item)
    }

    private fun onSelectedItemValueChanged(value: String?) {
        viewModel.onSelectedCurrencyValueChanged(value)
    }

    private fun injectDependencies(context: Context) {
        DaggerRatesComponent.builder()
            .ratesModule(RatesModule(context.applicationContext as Application))
            .build()
            .inject(this)

        viewModel = ViewModelProviders.of(this, viewModelProvider)
            .get(RatesListViewModel::class.java)
    }
}