package com.kazarovets.testapprevolut.ui.recycler

import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import kotlinx.android.synthetic.main.rates_item.view.*
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

class CurrencyInfoViewHolder(
    itemView: View,
    private val valueTextWatcher: TextWatcher
) : RecyclerView.ViewHolder(itemView) {

    fun bind(
        info: CurrencyViewInfo
    ) {
        itemView.ratesItemCode.text = info.currencyCode
        itemView.ratesItemImage.setImageResource(info.imageRes)
        itemView.ratesItemName.setText(info.currencyNameRes)

        setValue(info.value, info.isEditable)
    }

    private fun setValue(value: BigDecimal, isEditable: Boolean) {
        itemView.ratesItemValue.isEnabled = isEditable
        itemView.ratesItemValue.isClickable = isEditable

        itemView.ratesItemValue.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                itemView.ratesItemValue.addTextChangedListener(valueTextWatcher)
            } else {
                itemView.ratesItemValue.removeTextChangedListener(valueTextWatcher)
            }
        }


        if (isEditable) {
            itemView.ratesItemValue.isFocusableInTouchMode = true
        } else {
            itemView.ratesItemValue.isFocusable = false
        }

        val text = itemView.ratesItemValue.text.toString()
        val currDecimal = try {
            BigDecimal(text)
        } catch (e: Exception) {
            null
        }
        if (isEditable.not() || currDecimal == null || currDecimal.compareTo(value) != 0) {
            itemView.ratesItemValue.setText(value.toAppString())
        }
    }

    private fun BigDecimal.toAppString(): String {
        if(compareTo(BigDecimal.ZERO) == 0) return "0"

        val maxDecimalNumber = 5
        return this.setScale(maxDecimalNumber, RoundingMode.HALF_EVEN)
            .stripTrailingZeros()
            .toPlainString()
    }

    fun update(info: CurrencyViewInfo) {
        setValue(info.value, info.isEditable)
    }
}