package com.kazarovets.testapprevolut.ui.recycler

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.kazarovets.testapprevolut.R
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import com.kazarovets.testapprevolut.ui.recycler.RatesListDiffCallback.Companion.PAYLOAD_CHANGE_VALUE_AND_EDITABLE

class RatesListRecyclerAdapter(
    val onClick: (AppCurrency) -> Unit,
    val onSelectedItemValueChange: (String?) -> Unit
) : ListAdapter<CurrencyViewInfo, CurrencyInfoViewHolder>(RatesListDiffCallback()) {

    val valueTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            onSelectedItemValueChange(s?.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyInfoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.rates_item, parent, false)

        val holder = CurrencyInfoViewHolder(view, valueTextWatcher)
        view.setOnClickListener { onClick(getItem(holder.adapterPosition).currency) }

        return holder
    }

    override fun onBindViewHolder(
        holder: CurrencyInfoViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isNullOrEmpty().not()) {
            val item = getItem(position)
            if (payloads[0] == PAYLOAD_CHANGE_VALUE_AND_EDITABLE) {
                holder.update(item)
            }
        } else {
            onBindViewHolder(holder, position)
        }
    }

    override fun onBindViewHolder(holder: CurrencyInfoViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }
}