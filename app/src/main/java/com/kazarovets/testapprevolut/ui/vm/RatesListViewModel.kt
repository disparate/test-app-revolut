package com.kazarovets.testapprevolut.ui.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.logic.model.CurrencyRatesResponse
import com.kazarovets.testapprevolut.logic.usecase.GetCurrencyRatesInteractor
import com.kazarovets.testapprevolut.logic.usecase.SelectCurrencyInteractor
import com.kazarovets.testapprevolut.logic.usecase.UpdateCurrenciesInteractor
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import com.kazarovets.testapprevolut.ui.recycler.CurrenciesUpdate
import io.reactivex.disposables.Disposable
import java.math.BigDecimal

class RatesListViewModel(
    private val getRatesInteractor: GetCurrencyRatesInteractor,
    private val convertCurrencyInteractor: UpdateCurrenciesInteractor,
    private val selectCurrencyInteractor: SelectCurrencyInteractor,
    private val appSchedulers: AppSchedulers
) : ViewModel() {

    private val _currenciesInfos = MutableLiveData<CurrenciesUpdate>()
    val currenciesInfos: LiveData<CurrenciesUpdate> = _currenciesInfos

    private val _errorDownloadingRates = MutableLiveData<Boolean>().apply {
        value = false
    }
    val errorDownloadingRates: LiveData<Boolean> = _errorDownloadingRates

    private var selectedCurrency: AppCurrency = selectCurrencyInteractor.getStartSelectedCurrency()
    private var currencies: List<CurrencyViewInfo> =
        getRatesInteractor.initCurrencyRatesValues(selectedCurrency)
    private var ratesList: List<CurrencyRate> = emptyList()

    private var getRatesDisposable: Disposable? = null

    init {
        _currenciesInfos.value = CurrenciesUpdate(currencies)

        getRatesDisposable = getRatesInteractor.getRatesStream()
            .map { response ->
                if (response is CurrencyRatesResponse.Success) {
                    ratesList = response.rates
                    convertCurrencyInteractor.updateCurrenciesWithNewRates(
                        selectedCurrency, currencies, response.rates
                    ).also {
                        currencies = it
                        _currenciesInfos.postValue(CurrenciesUpdate(it))
                    }
                }
                _errorDownloadingRates.postValue(response is CurrencyRatesResponse.ErrorLoading)
            }
            .observeOn(appSchedulers.uiScheduler())
            .subscribe()
    }

    fun onItemClick(currency: AppCurrency) {
        if (currency == selectedCurrency) return

        val update = selectCurrencyInteractor.selectCurrency(
            currency = currency,
            prevSelected = selectedCurrency,
            currencies = currencies
        )

        selectedCurrency = currency

        update?.let {
            currencies = it.infos
            _currenciesInfos.value = it
        }
    }

    fun onSelectedCurrencyValueChanged(changed: String?) {
        val newValue = try {
            changed?.toBigDecimal() ?: BigDecimal.ZERO
        } catch (e: Exception) {
            BigDecimal.ZERO
        }

        convertCurrencyInteractor.updateCurrenciesWithNewSelectedValue(
            selectedCurrency, newValue, currencies, ratesList
        ).also {
            currencies = it
            _currenciesInfos.value = CurrenciesUpdate(it)
        }
    }

    public override fun onCleared() {
        super.onCleared()
        getRatesDisposable?.dispose()
    }
}