package com.kazarovets.testapprevolut.ui.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import java.math.BigDecimal

data class CurrencyViewInfo(
    val currency: AppCurrency,
    @StringRes val currencyNameRes: Int,
    @DrawableRes val imageRes: Int,
    val value: BigDecimal,
    val isEditable: Boolean
) {

    val currencyCode: String = currency.code

    fun copyWithValue(newValue: BigDecimal): CurrencyViewInfo {
        return this.copy(value = newValue)
    }
}