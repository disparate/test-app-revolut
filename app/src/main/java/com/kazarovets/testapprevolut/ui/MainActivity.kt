package com.kazarovets.testapprevolut.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kazarovets.testapprevolut.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}
