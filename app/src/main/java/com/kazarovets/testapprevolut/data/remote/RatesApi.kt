package com.kazarovets.testapprevolut.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    @GET("latest")
    fun getLatestRates(@Query("base") baseCurrency: String): Single<RatesRemoteResponse>


    companion object {
        const val CONNECTION_TIMEOUT_SEC = 5L
        const val READ_TIMEOUT_SEC = 5L
        const val WRITE_TIMEOUT_SEC = 5L

        const val BASE_URL = "https://revolut.duckdns.org/"
    }
}