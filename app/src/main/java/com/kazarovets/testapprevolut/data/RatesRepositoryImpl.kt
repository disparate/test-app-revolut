package com.kazarovets.testapprevolut.data

import com.kazarovets.testapprevolut.data.local.RatesLocalDataSource
import com.kazarovets.testapprevolut.data.remote.RatesRemoteDataSource
import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import io.reactivex.Observable
import io.reactivex.Single
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

@RatesScope
class RatesRepositoryImpl @Inject constructor(
    private val ratesRemoteDataSource: RatesRemoteDataSource,
    private val ratesLocalDataSource: RatesLocalDataSource
) : RatesRepository {

    override fun downloadAndSaveCurrencyRates(base: AppCurrency): Single<List<CurrencyRate>> {
        return ratesRemoteDataSource.downloadCurrencyRates(base)
            .doAfterSuccess { ratesLocalDataSource.saveCurrencyRates(it) }
    }

    override fun getSavedCurrencyRates(): Single<List<CurrencyRate>> {
        return ratesLocalDataSource.getCurrencyRates()
    }

    override fun saveSelectedCurrency(currency: AppCurrency) {
        ratesLocalDataSource.saveSelectedCurrency(currency)
    }

    override fun getSavedSelectedCurrency(): AppCurrency? {
        return ratesLocalDataSource.getSelectedCurrency()
    }
}