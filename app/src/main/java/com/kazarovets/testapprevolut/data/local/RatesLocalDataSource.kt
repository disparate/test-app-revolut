package com.kazarovets.testapprevolut.data.local

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject


class RatesLocalDataSource @Inject constructor(
    private val prefs: RxSharedPreferences
) {

    private val parser = JsonParser()

    private val selectedCurrencyCode = prefs.getString("selected_currency")

    private val currencyRatesPref = prefs.getStringSet("currency_rates")

    fun saveSelectedCurrency(appCurrency: AppCurrency) {
        selectedCurrencyCode.set(appCurrency.code)
    }

    fun getSelectedCurrency(): AppCurrency? {
        return codeToCurrency(selectedCurrencyCode.get())
    }


    fun saveCurrencyRates(rates: List<CurrencyRate>) {
        val set = rates.map {
            JsonObject().apply {
                addProperty(CURRENCY_RATE_CURRENCY, it.currency.code)
                addProperty(CURRENCY_RATE_BASE, it.base.code)
                addProperty(CURRENCY_RATE_VALUE, it.value.toString())
            }
        }
            .map { it.toString() }
            .toSet()
        currencyRatesPref.set(set)
    }

    fun getCurrencyRates(): Single<List<CurrencyRate>> {
        return Single.fromCallable { getCurrencyRatesSync() }
            .subscribeOn(Schedulers.io())
    }

    private fun getCurrencyRatesSync(): List<CurrencyRate> {
        return currencyRatesPref.get().mapNotNull {
            val json = parser.parse(it) as JsonObject

            CurrencyRate(
                codeToCurrency(json.get(CURRENCY_RATE_CURRENCY).asString) ?: return@mapNotNull null,
                codeToCurrency(json.get(CURRENCY_RATE_BASE).asString) ?: return@mapNotNull null,
                BigDecimal(json.get(CURRENCY_RATE_VALUE).asString)
            )
        }
    }

    private fun codeToCurrency(code: String?): AppCurrency? {
        return AppCurrency.values().find { it.code == code }
    }

    private companion object {
        const val CURRENCY_RATE_CURRENCY = "currency"
        const val CURRENCY_RATE_BASE = "base"
        const val CURRENCY_RATE_VALUE = "value"
    }
}