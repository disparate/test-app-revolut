package com.kazarovets.testapprevolut.data

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface AppSchedulers {
    fun ioScheduler(): Scheduler
    fun computationScheduler(): Scheduler
    fun uiScheduler(): Scheduler
}

class SchedulersImpl(): AppSchedulers {
    override fun ioScheduler(): Scheduler {
        return Schedulers.io()
    }

    override fun computationScheduler(): Scheduler {
        return Schedulers.computation()
    }

    override fun uiScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

}