package com.kazarovets.testapprevolut.data

import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import io.reactivex.Single

interface RatesRepository {

    fun getSavedSelectedCurrency(): AppCurrency?

    fun saveSelectedCurrency(currency: AppCurrency)

    fun getSavedCurrencyRates(): Single<List<CurrencyRate>>

    fun downloadAndSaveCurrencyRates(base: AppCurrency): Single<List<CurrencyRate>>
}