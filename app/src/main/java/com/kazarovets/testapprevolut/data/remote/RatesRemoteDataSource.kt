package com.kazarovets.testapprevolut.data.remote

import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject

@RatesScope
class RatesRemoteDataSource @Inject constructor(
    private val api: RatesApi
) {

    fun downloadCurrencyRates(base: AppCurrency): Single<List<CurrencyRate>> {
        return api.getLatestRates(base.code)
            .subscribeOn(Schedulers.io())
            .map { response ->
                response.ratesByCodes.orEmpty()
                    .mapNotNull {
                        val appCurrency = getAppCurrency(it.key) ?: return@mapNotNull null
                        CurrencyRate(appCurrency, base, it.value)
                    }
                    .toMutableList().apply { add(0, CurrencyRate(base, base, BigDecimal.ONE)) }
            }
    }

    private fun getAppCurrency(code: String): AppCurrency? {
        val supportedCurrencies = AppCurrency.values()
        return supportedCurrencies.find { it.code == code }
    }
}