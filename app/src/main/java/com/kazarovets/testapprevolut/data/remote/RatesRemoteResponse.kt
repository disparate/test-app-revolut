package com.kazarovets.testapprevolut.data.remote

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

class RatesRemoteResponse(
    @SerializedName("rates")
    val ratesByCodes: Map<String, BigDecimal>? = null
)