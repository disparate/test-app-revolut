package com.kazarovets.testapprevolut.di.rates

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.data.RatesRepository
import com.kazarovets.testapprevolut.data.RatesRepositoryImpl
import com.kazarovets.testapprevolut.data.SchedulersImpl
import com.kazarovets.testapprevolut.data.local.RatesLocalDataSource
import com.kazarovets.testapprevolut.data.remote.RatesRemoteDataSource
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers

@Module
class RatesModule(private val app: Application) {

    @Provides
    @RatesScope
    fun provideApplication(): Application {
        return app
    }

    @Provides
    @RatesScope
    fun repo(
        remote: RatesRemoteDataSource,
        local: RatesLocalDataSource
    ): RatesRepository {
        return RatesRepositoryImpl(remote, local)
    }

    @Provides
    @RatesScope
    fun schedulers(): AppSchedulers {
        return SchedulersImpl()
    }

    @Provides
    @RatesScope
    fun prefs(app: Application): RxSharedPreferences {
        return RxSharedPreferences.create(
            app.getSharedPreferences("rates", Context.MODE_PRIVATE)
        )
    }
}