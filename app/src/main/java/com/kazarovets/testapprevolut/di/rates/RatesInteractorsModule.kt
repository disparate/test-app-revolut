package com.kazarovets.testapprevolut.di.rates

import com.kazarovets.testapprevolut.logic.usecase.*
import dagger.Binds
import dagger.Module

@Module
abstract class RatesInteractorsModule {

    @Binds
    @RatesScope
    abstract fun getRates(impl: GetCurrencyRatesInteractorImpl): GetCurrencyRatesInteractor

    @Binds
    @RatesScope
    abstract fun updateCurrencies(impl: UpdateCurrenciesInteractorImpl): UpdateCurrenciesInteractor

    @Binds
    @RatesScope
    abstract fun selectCurrency(impl: SelectCurrencyInteractorImpl): SelectCurrencyInteractor
}