package com.kazarovets.testapprevolut.di.rates

import com.kazarovets.testapprevolut.ui.RatesListFragment
import dagger.Component

@RatesScope
@Component(modules = [RatesModule::class, RatesInteractorsModule::class, RatesNetworkModule::class])
interface RatesComponent {

    fun inject(fragment: RatesListFragment)
}