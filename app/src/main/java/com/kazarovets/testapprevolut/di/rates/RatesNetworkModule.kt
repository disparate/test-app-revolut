package com.kazarovets.testapprevolut.di.rates

import com.kazarovets.testapprevolut.data.remote.RatesApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class RatesNetworkModule {

    @Provides
    fun retrofit(): Retrofit {
        val httpClient = OkHttpClient.Builder()

        httpClient
            .connectTimeout(RatesApi.CONNECTION_TIMEOUT_SEC, TimeUnit.SECONDS)
            .readTimeout(RatesApi.READ_TIMEOUT_SEC, TimeUnit.SECONDS)
            .writeTimeout(RatesApi.WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)

        //Build
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .baseUrl(RatesApi.BASE_URL)
            .build()
    }

    @Provides
    @RatesScope
    fun ratesApi(retrofit: Retrofit): RatesApi {
        return retrofit.create(RatesApi::class.java)
    }
}