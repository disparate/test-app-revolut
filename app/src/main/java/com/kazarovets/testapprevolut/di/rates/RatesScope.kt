package com.kazarovets.testapprevolut.di.rates

import javax.inject.Scope

@Scope
annotation class RatesScope