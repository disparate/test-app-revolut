package com.kazarovets.testapprevolut.logic.usecase

import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import java.math.BigDecimal
import javax.inject.Inject


interface UpdateCurrenciesInteractor {
    fun updateCurrenciesWithNewRates(
        selectedCurrency: AppCurrency,
        currCurrencies: List<CurrencyViewInfo>,
        rates: List<CurrencyRate>
    ): List<CurrencyViewInfo>

    fun updateCurrenciesWithNewSelectedValue(
        selectedCurrency: AppCurrency,
        newSelectedValue: BigDecimal,
        currCurrencies: List<CurrencyViewInfo>,
        rates: List<CurrencyRate>
    ): List<CurrencyViewInfo>
}

@RatesScope
class UpdateCurrenciesInteractorImpl @Inject constructor(
    private val currencyConverter: CurrencyConverter
): UpdateCurrenciesInteractor {

    override fun updateCurrenciesWithNewRates(
        selectedCurrency: AppCurrency,
        currCurrencies: List<CurrencyViewInfo>,
        rates: List<CurrencyRate>
    ): List<CurrencyViewInfo> {
        val selectedCurrencyValue = currCurrencies.find { it.currency == selectedCurrency }?.value
            ?: return currCurrencies

        return updateCurrencies(selectedCurrency, selectedCurrencyValue, currCurrencies, rates)
    }

    override fun updateCurrenciesWithNewSelectedValue(
        selectedCurrency: AppCurrency,
        newSelectedValue: BigDecimal,
        currCurrencies: List<CurrencyViewInfo>,
        rates: List<CurrencyRate>
    ): List<CurrencyViewInfo> {
        return updateCurrencies(selectedCurrency, newSelectedValue, currCurrencies, rates)
    }

    private fun updateCurrencies(
        selectedCurrency: AppCurrency,
        newSelectedValue: BigDecimal,
        currCurrencies: List<CurrencyViewInfo>,
        rates: List<CurrencyRate>
    ): List<CurrencyViewInfo> {
        val selectedCurrencyRate = rates.find { it.currency == selectedCurrency }
            ?: return currCurrencies

        return currCurrencies.mapNotNull { c ->
            if (c.currency == selectedCurrency) {
                c.copyWithValue(newSelectedValue)
            } else {
                val rate = rates.find { c.currency == it.currency } ?: return@mapNotNull null

                val new = currencyConverter.convertCurrency(
                    newSelectedValue, selectedCurrencyRate, rate
                )

                c.copyWithValue(new)
            }
        }
    }
}