package com.kazarovets.testapprevolut.logic.model

import java.math.BigDecimal

data class CurrencyRate(
    val currency: AppCurrency,
    val base: AppCurrency,
    val value: BigDecimal
)