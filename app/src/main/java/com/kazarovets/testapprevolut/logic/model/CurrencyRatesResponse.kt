package com.kazarovets.testapprevolut.logic.model

sealed class CurrencyRatesResponse {
    data class Success(val rates: List<CurrencyRate>) : CurrencyRatesResponse()
    data class ErrorLoading(val t: Throwable) : CurrencyRatesResponse()
}