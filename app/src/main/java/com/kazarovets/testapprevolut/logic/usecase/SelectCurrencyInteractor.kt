package com.kazarovets.testapprevolut.logic.usecase

import com.kazarovets.testapprevolut.data.RatesRepository
import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import com.kazarovets.testapprevolut.ui.recycler.CurrenciesUpdate
import javax.inject.Inject

interface SelectCurrencyInteractor {
    fun selectCurrency(
        currency: AppCurrency,
        prevSelected: AppCurrency,
        currencies: List<CurrencyViewInfo>
    ): CurrenciesUpdate?

    fun getStartSelectedCurrency(): AppCurrency
}

@RatesScope
class SelectCurrencyInteractorImpl @Inject constructor(
    private val repository: RatesRepository
) : SelectCurrencyInteractor {

    override fun selectCurrency(
        currency: AppCurrency,
        prevSelected: AppCurrency,
        currencies: List<CurrencyViewInfo>
    ): CurrenciesUpdate? {

        if (currency == prevSelected) {
            return null
        }

        val selectedInfo = currencies.find { it.currency == currency }
            ?.copy(
                isEditable = true
            ) ?: return null

        val currencies = currencies.map {
            if (it.isEditable) it.copy(isEditable = false) else it
        }
            .toMutableList()
            .apply {
                removeAll { it.currency == currency }
                add(0, selectedInfo)
            }

        repository.saveSelectedCurrency(currency)

        return CurrenciesUpdate(currencies, true)
    }


    override fun getStartSelectedCurrency(): AppCurrency {
        return repository.getSavedSelectedCurrency() ?: AppLogicConstants.baseCurrency
    }
}