package com.kazarovets.testapprevolut.logic.usecase

import com.kazarovets.testapprevolut.data.AppSchedulers
import com.kazarovets.testapprevolut.data.RatesRepository
import com.kazarovets.testapprevolut.di.rates.RatesScope
import com.kazarovets.testapprevolut.logic.model.AppCurrency
import com.kazarovets.testapprevolut.logic.model.CurrencyRatesResponse
import com.kazarovets.testapprevolut.ui.model.CurrencyViewInfo
import com.kazarovets.testapprevolut.ui.vm.CurrencyViewInfoMapper
import io.reactivex.Observable
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface GetCurrencyRatesInteractor {
    fun getRatesStream(): Observable<CurrencyRatesResponse>
    fun initCurrencyRatesValues(selected: AppCurrency): List<CurrencyViewInfo>
}

@RatesScope
class GetCurrencyRatesInteractorImpl @Inject constructor(
    private val ratesRepository: RatesRepository,
    private val currencyMapper: CurrencyViewInfoMapper,
    private val schedulers: AppSchedulers
): GetCurrencyRatesInteractor {

    override fun getRatesStream(): Observable<CurrencyRatesResponse> {
        return Observable
            .interval(0L, UPDATE_PERIOD_MS, TimeUnit.MILLISECONDS, schedulers.computationScheduler())
            .flatMap {
                ratesRepository
                    .downloadAndSaveCurrencyRates(AppLogicConstants.baseCurrency)
                    .toObservable()
                    .map<CurrencyRatesResponse> { CurrencyRatesResponse.Success(it) }
                    .onErrorReturn { CurrencyRatesResponse.ErrorLoading(it)}
            }
            .startWith(initCurrencyRatesObservable())
            .distinctUntilChanged()
            .throttleLatest(1, TimeUnit.SECONDS, schedulers.computationScheduler())
    }

    private fun initCurrencyRatesObservable(): Observable<CurrencyRatesResponse> {
        return ratesRepository.getSavedCurrencyRates()
            .filter { it.isEmpty().not() }
            .onErrorComplete()
            .toObservable()
            .map { CurrencyRatesResponse.Success(it) }
    }

    override fun initCurrencyRatesValues(selected: AppCurrency): List<CurrencyViewInfo> {
        val list = AppCurrency.values().toMutableList()
        list.remove(selected)
        list.add(0, selected)

        return list.mapIndexed { i, currency ->
            val value = if (currency == selected) INIT_SELECTED_VALUE else BigDecimal.ZERO
            currencyMapper.mapToViewInfo(currency, value, currency == selected)
        }
    }

    companion object {
        const val UPDATE_PERIOD_MS = 1000L
        val INIT_SELECTED_VALUE = BigDecimal("100")
    }
}