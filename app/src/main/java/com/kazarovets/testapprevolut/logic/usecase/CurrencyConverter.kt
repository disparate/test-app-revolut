package com.kazarovets.testapprevolut.logic.usecase

import com.kazarovets.testapprevolut.logic.model.CurrencyRate
import java.lang.IllegalArgumentException
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

class CurrencyConverter @Inject constructor() {

    fun convertCurrency(
        selectedCurrencyValue: BigDecimal,
        selectedCurrencyRate: CurrencyRate,
        currencyRate: CurrencyRate
    ): BigDecimal {
        if (selectedCurrencyRate.base != currencyRate.base) throw IllegalArgumentException("base currency must be same")

        if(selectedCurrencyRate.value.compareTo(BigDecimal.ZERO) == 0) throw IllegalArgumentException("selected rate must not be equal to zero")

        val defaultVal =
            selectedCurrencyValue.divide(selectedCurrencyRate.value, 5, RoundingMode.HALF_UP)
        return currencyRate.value.multiply(defaultVal).setScale(7, RoundingMode.HALF_UP)
    }
}