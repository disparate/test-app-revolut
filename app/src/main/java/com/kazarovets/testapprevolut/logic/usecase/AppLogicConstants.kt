package com.kazarovets.testapprevolut.logic.usecase

import com.kazarovets.testapprevolut.logic.model.AppCurrency

object AppLogicConstants {

    val baseCurrency: AppCurrency = AppCurrency.EURO
}